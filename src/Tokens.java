public enum Tokens
{
	ADD("+"),
	SUB("-"),
	DIVIDE("/"),
	MULTIPLY("*"),
	/* Numeric */
	ONE("1"),
	TWO("2"),
	THREE("3"),
	FOUR("4"),
	FIVE("5"),
	SIX("6"),
	SEVEN("7"),
	EIGHT("8"),
	NINE("9"),
	ZERO("0");
	
	public final String tokenName;
	
	Tokens(String tokenName) {
		this.tokenName = tokenName;
	}
}
