
public class BinaryOperation implements Token {
	private String optIdentifier;
	
	public BinaryOperation(String str) {
		this.optIdentifier = str;
	}
	
	public String getOperation()
	{
		return this.optIdentifier;
	}
	
	public String type() {
		return "b-operation";
	}
	
	public String toString() {
		return this.type();
	}
}
