import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Scanner {
	// global field
	// parsedTokensStack works like stack
	private ArrayList<Token> parsedTokensStack = new ArrayList<Token>();
	
	// binary opts
	private final String[] binaryOpt = {"add", "subtract", "multiply", "divide"};

	public void scan(String args[]) {
		int i;
		StringBuffer input = new StringBuffer();
		input.append(args[0]);
		for (i = 0; i < args.length; i++) {
			input.append(" " + args[i]);
		}
		
		boolean isValidate = validate(input.toString());
		
		if (isValidate) {
			i = 0;
			for (i = 0; i < args.length; i++) {
				String str 	= args[i];
				if (isNumeric(str)) {
					// push numeric object
					Numeric number = new Numeric(str);
					parsedTokensStack.add(number);
				}
				else if (isBinaryOperation(str)) {
					// push binary object
					BinaryOperation bOpt = new BinaryOperation(str);
					parsedTokensStack.add(bOpt);
				}
			}
			
			System.out.println(parsedTokensStack.toString());
		}
	}
	
	// checks for is string input value represent as binary operation group
	private boolean isBinaryOperation(String str) {
		for (int i = 0; i < binaryOpt.length; i++) {
			if (str.equalsIgnoreCase(binaryOpt[i]))
				return true;
		}
		
		return false;
	}

	// checks for is string input value represent as integer value
	public boolean isNumeric(String str) {
		// from stackoverflow.com
		// http://stackoverflow.com/questions/5439529/determine-if-a-string-is-an-integer-in-java
		Pattern p = Pattern.compile("(negative)?(zero|one|two|three|four|five|six|seven|eight|nine)", Pattern.CASE_INSENSITIVE);
		return p.matcher(str).matches();
	}

	public boolean validate(String inputStr) {
		System.out.println(inputStr);
		// pattern regex
		Pattern p = Pattern.compile("((zero|one|two|three|four|five|six|seven|eight|nine)\\s){1,}(add|sub|divide|multiply)(\\s(zero|one|two|three|four|five|six|seven|eight|nine)){1,}", Pattern.CASE_INSENSITIVE);
		// input string
		Matcher m = p.matcher(inputStr);
		System.out.println(inputStr + " " + m.matches());
		return m.matches();
	}
}