
public class Numeric implements Token {
	
	private String numberStr;
	
	public Numeric(String str) {
		this.numberStr = str;
	}
	
	public String getNumber() {
		return this.numberStr;
	}
	
	public String type() {
		return "number";
	}
	
	public String toString() {
		return String.format("%s:%s", this.type(), this.numberStr);
	}
}
